========================================================================
ISSUES-GAME: Gameplay-affecting issues
------------------------------------------------------------------------

  ####   #####    ####      #    #       ######  #####    ####
 #       #    #  #    #     #    #       #       #    #  #
  ####   #    #  #    #     #    #       #####   #    #   ####
      #  #####   #    #     #    #       #       #####        #
 #    #  #       #    #     #    #       #       #   #   #    #
  ####   #        ####      #    ######  ######  #    #   ####

- Continuing issues with storebox fall-thru
	- Items now refusing to fall from a form into a crate or
	  case directly below.
	- Prills dropping out of glowing lode spawn inside a
	  storebox below, but this allows them to fall through
	  the bottom (since they're already past colliding with
	  the box and being absorbed)
		- Maybe entities need to try to settle into a box
		  if it WAS in the box's node space last tick but
		  is below it now, and the box isn't open on
		  the bottom.

- /snuff should probably snuff torches too
	- General snuffable/on_snuff API?
	- Merge with growtrees for a general on_cheat API?

- Detect and warn players about misuse of singlenode mapgen
	- Mods can register if they're supporting singlenode.
	- We can detect player "falling forever" by scanning down
	  to find anything other than air before ignore.

- Instead of always displacing nodes upon falling node landing, should
  check for diggability and if diggable, convert to drops.

- Touch-damage is overused, should probably only apply to
  obviously extremely hot things like embers and pumwater.
	- Maybe add a pickup_damage/hotpotato group specifically
	  for the hot potato functionality.

- Re-examine chiseling recipes
	- Bricks accept annealed chisels, which makes some sense
	  from a game balance perspective (cosmetic-only = cheap)
	  but less re: realism.
	- Similarly doors all require tempered chisels, and maybe
	  at least wooden doors should accept annealed.

- Cannot catapult an item into a form from below.
	- Is this actually important?

- Thatch and wicker should probably have recycling recipes.

- Do we really need stack_as_node for leaves, sticks, and a
  few other ordinary items anymore, or has our falling_node
  rework made that irrelevant now?

- Grass, sprouts, and stumps (maybe others) all reuse textures from
  dirt, and from some sides are visually indistinguishable from
  dirt, yet the player is somehow able to distinguish them via a
  looktip (not even needing to touch/hear).  This feels inconsistent
  and maybe these nodes should have distinct textures on all sides,
  like added roots.

- Sponge Fixes
	- Fix crappy sponge death mechanic
		- Remove special treatment of glass case and glass nodes
			- May need to find a new use-case for glass case?
	- Improve sponge colony growth shape
		- Prevent tall vertical column growth seen on NCC by Kimapr
		- Instead of just pushing out a sponge, floodfill to find colony
		(reuse colony volume check), then pick a random surface node
		to grow from; maybe save candidate water spots during the flood
		scan and grow non-locally to that?

- Hint system:
	- Split hint descriptions into "pre" and "post" parts.
		- Undiscovered, show just "pre" part (1 stick + 4 adze)
		- After, show both (1 stick + 4 adze => rake)
	- Make all crafts/recipes trigger witnessing?
	- Hide stylus hint until after mixed concrete

- Make a way to prevent crafting on place
	- Sneak? Aux?
	- Confirmation UI after completing recipe, e.g. floating ent
	- Make all recipes include some pummel factor?

- Doors should produce particles when digging or pummeling

- Door press recipe limitations
	- Node-placing recipes don't work with itemstacks
		- Tool and torch making don't work
		- We should be able to detect failed-catapulting for this
	- Stackapply recipes
		- Needed for automatic eggcorn planting

- Health system slot bias should be a bit stronger

- Consider making offline (e.g. soaking) growth rates sublinear
  with time, e.g. diminishing returns for offline time, to create
  incentives to build nearby

- Stone softening rates feel off.
	- Once first stage of stone softens, others follow really
	  quickly (exponential runaway basically).
	- If you leave stone soaking for a while, it looks like it
	  basically softens all or nothing.
	- Can we smooth this out and make it less jarring?

- Item ent fixes
	- Make crush damage use velocity relative to player,
	  not absolute velocity, to calculate damage

- A way to visualize the infusion rate effects would be nice
	- Particles don't work due to semitransparent liquid

- Make door operation bredth-first queue-recursive for consistent
  operation and addressing timing conflicts.

- Remove fullbright from admin hand.
	- Separate fullbright priv.
		- Also disable skybox/sunlight diminishing by depth?
	- The hand could use a bit of an overhaul too.
		- Better name, appearance.
		- Make it look like a tool instead of hand?
		- Maybe some kind of force entity like a lighting bolt?

- Add a way to extend wandering generations
	- For glass, heat sources
	- For concrete, moisture sources

- Rakes should probably not charge durability for digging first item
  stack, or should charge reduced wear for item stacks.

- Tarstone should produce coal particles consistently when broken back
  down to regular cobble, but it only works with the base node, not the
  etched ones.

- Integrate YCTIWY into base game

- Players losing run speed on damage would be simplified by checking for
  <max health instead of time of last damage.
	- Should lose farzoom when damaged too.

- Should water/lava springs vary by depth?  Lava seems kind of laborious
  to find right now.

- We can reliably detect the zoom player control, make far-zoom only
  work while zooming, and add hints for it.

- Door upward conveying is sort of useless right now; make it also
  push objects outward, so this motion can be useful for upward
  item transport?

- Valleys mapgen rivers seem to rely on having an actual separate
  river water registration with a specific liquid range.

- Should item ents experience more drag when in fluids?

- Door-wheels do not affect attached glyphs...

- Add a few smoke particles to fire?
	- Make them different (smaller?) than cooking to differentiate.

- Consider changing wet/dry stack splitting
	- Currently, original stack remains in place, wetted/dried
	  stack jumps out to new spot.
	- Instead, maybe wetted/dried stays in place and original
	  jumps out (unless there's an adjacent wetted/dried stack to
	  reuse).
	- More similar to the way flammable stack ignition works
	- Just meaner overall

- Consistentize soaking/offline mechanics
	- Use a DNT to make check regular for things actually soaking
	- To consider:
		- Tree growth
		- Compost
		- Sponge expiration
		- Torch expiration
		- Leaching
		- Repacking
		- Concrete
		- Lux renew
	- Logic: Action by a living thing (i.e. presence of player or
	  microorganisms) is what keeps time ticking...?
	- Be more consistent with some non-offline mechanics like
	  fire fuel consumption
	- When we did torches via expiration metadata, the reason at the
	  time was that there was no way to get them to tick reliably like
	  we can with nodes; with AISMs this is no longer the case.

- Repose changes
	- Staves/rods should repose a lot less or not at all.
	- Rework repose to be based on amount of loose material below, not
	  amount of distance available to fall?  repose only needs to have a
	  particular angle as measured on flat land?

- Delay lens full brightness?
	- Propagate optic signals immediately
	- Reach full brightness a few seconds later, to reduce map
	  update work and prevent rapid strobes.
	- Strobes can cause excess lighting recalcs and block
	  transfers, and can cause seizure problems for users

- Tote issues:
	- Consider updating tote recipe.
	- Totes should do face-connected search for totables; don't pick up
	  shelves only touching via a corner.

- Flammables should respond via AISM when bathed in fire or igniting
  liquids like molten glass or rock.

- Should there be pockets of softer stone at depth, like there is gravel?
	- Register ores for softer stone of each level
	- Maybe some ores for cobble and/or loose cobble?
	- Maybe ores for dirt, sand?

- Consider checking for a nearby active player before running
  "fast-evolving" ABMs that don't normally work offline, like fire
  extinguishing/spreading?
	- This would make gameplay more fair for those who tend to
	  go AFK suddenly and can't return for a long time.
	- This would reduce the gap between things that support the offline
	  mechanic and those that don't between MP and SP.

- Make separate walkable/non-walkable stack nodes.
	- Should sticks and eggcorns be non-walkable?

........................................................................
========================================================================
