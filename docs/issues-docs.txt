========================================================================
ISSUES-DOCS: Issues related to documentation
------------------------------------------------------------------------

  ####   #####    ####      #    #       ######  #####    ####
 #       #    #  #    #     #    #       #       #    #  #
  ####   #    #  #    #     #    #       #####   #    #   ####
      #  #####   #    #     #    #       #       #####        #
 #    #  #       #    #     #    #       #       #   #   #    #
  ####   #        ####      #    ######  ######  #    #   ####

- Official screenshot missing:
	- Thatch
	- Wicker
	- Raked sand/dirt/gravel/humus

- Add a credits file with more extensive/specific credits, including
  non-copyrightable contributions to core project...?

- Need to clean out issues in wishlist.
	- Put some issues unlikely to be resolved on ice.

- Consider migrating NC's Weblate into the hosted instance that
  Minetest Engine uses?

- Documentation about accessibility
	- Accessibility features
		- All significant actions have sounds
		- All sounds should have visuals (subtitle branch?)
		- All text translatable
		- Input speed limits, e.g. Mobile and RSI
	- Recommend user settings
		- Gamma/lighting
		- Enlarge HUDs and GUIs

........................................................................
========================================================================
