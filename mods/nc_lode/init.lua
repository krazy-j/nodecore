-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("ore")
include("metallurgy")
include("anvils")
include("oresmelt")
include("tools")
include("shafts")
include("ladder")
include("adze")
include("rake")
include("shelf")
include("hints")
