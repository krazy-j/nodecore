msgid "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr "- "Furnaces" are not a thing; discover smelting with open flames."

msgid "- @1"
msgstr "- @1"

msgid "- Aux+drop any item to drop everything."
msgstr "- Aux+drop any item to drop everything."

msgid "- Be wary of dark caves/chasms; you are responsible for getting yourself out."
msgstr "- Be wary of dark caves/chasms; you are responsible for getting yourself out."

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr "- Can't dig trees or grass? Search for sticks in the canopy."

msgid "- Climbing spots also produce very faint light; raise display gamma to see."
msgstr "- Climbing spots also produce very faint light; raise display gamma to see."

msgid "- Climbing spots may be climbed once black particles appear."
msgstr "- Climbing spots may be climbed once black particles appear."

msgid "- Crafting is done by building recipes in-world."
msgstr "- Crafting is done by building recipes in-world."

msgid "- DONE: @1"
msgstr "- DONE: @1"

msgid "- Displaced nodes can be climbed through like climbing spots."
msgstr "- Displaced nodes can be climbed through like climbing spots."

msgid "- Do not use F5 debug info; it will mislead you!"
msgstr "- Do not use F5 debug info; it will mislead you!"

msgid "- Drop and pick up items to rearrange your inventory."
msgstr "- Drop and pick up items to rearrange your inventory."

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr "- Drop items onto ground to create stack nodes. They do not decay."

msgid "- FUTURE: @1"
msgstr "- FUTURE: @1"

msgid "- For larger recipes, the center item is usually placed last."
msgstr "- For larger recipes, the center item is usually placed last."

msgid "- Hold/repeat right-click on walls/ceilings barehanded to climb."
msgstr "- Hold/repeat right-click on walls/ceilings barehanded to climb."

msgid "- Hopelessly stuck? Try asking the community chatrooms (About tab)."
msgstr "- Hopelessly stuck? Try asking the community chatrooms (About tab)."

msgid "- If a recipe exists, you will see a special particle effect."
msgstr "- If a recipe exists, you will see a special particle effect."

msgid "- If it takes more than 5 seconds to dig, you don't have the right tool."
msgstr "- If it takes more than 5 seconds to dig, you don't have the right tool."

msgid "- Items picked up try to fit into the current selected slot first."
msgstr "- Items picked up try to fit into the current selected slot first."

msgid "- Larger recipes are usually more symmetrical."
msgstr "- Larger recipes are usually more symmetrical."

msgid "- Learn to use the stars for long distance navigation."
msgstr "- Learn to use the stars for long distance navigation."

msgid "- Nodes dug without the right tool cannot be picked up, only displaced."
msgstr "- Nodes dug without the right tool cannot be picked up, only displaced."

msgid "- Order and specific face of placement may matter for crafting."
msgstr "- Order and specific face of placement may matter for crafting."

msgid "- Ores may be hidden, but revealed by subtle clues in terrain."
msgstr "- Ores may be hidden, but revealed by subtle clues in terrain."

msgid "- Recipes are time-based, punching faster does not speed up."
msgstr "- Recipes are time-based, punching faster does not speed up."

msgid "- Sneak+aux+drop an item to drop all matching items."
msgstr "- Sneak+aux+drop an item to drop all matching items."

msgid "- Sneak+drop to count out single items from stack."
msgstr "- Sneak+drop to count out single items from stack."

msgid "- Some recipes require "pummeling" a node."
msgstr "- Some recipes require "pummeling" a node."

msgid "- Something seems tedious? Find better tech, subtle factors, or a better way."
msgstr "- Something seems tedious? Find better tech, subtle factors, or a better way."

msgid "- Stacks may be pummeled, exact item count may matter."
msgstr "- Stacks may be pummeled, exact item count may matter."

msgid "- The game is challenging by design, sometimes frustrating. DON'T GIVE UP!"
msgstr "- The game is challenging by design, sometimes frustrating. DON'T GIVE UP!"

msgid "- There is NO inventory screen."
msgstr "- There is NO inventory screen."

msgid "- To pummel, punch a node repeatedly, WITHOUT digging."
msgstr "- To pummel, punch a node repeatedly, WITHOUT digging."

msgid "- To run faster, walk/swim forward or climb/swim upward continuously."
msgstr "- To run faster, walk/swim forward or climb/swim upward continuously."

msgid "- Tools used as ingredients must be in very good condition."
msgstr "- Tools used as ingredients must be in very good condition."

msgid "- Trouble lighting a fire? Try using longer sticks, more tinder."
msgstr "- Trouble lighting a fire? Try using longer sticks, more tinder."

msgid "- Wielded item, target face, and surrounding nodes may matter."
msgstr "- Wielded item, target face, and surrounding nodes may matter."

msgid "- You do not have to punch very fast (about 1 per second)."
msgstr "- You do not have to punch very fast (about 1 per second)."

msgid "@1 (@2)"
msgstr "@1 (@2)"

msgid "@1 ....."
msgstr "@1 ....."

msgid "@1 discovered, @2 available, @3 future"
msgstr "@1 discovered, @2 available, @3 future"

msgid "@1 |...."
msgstr "@1 |...."

msgid "@1 ||..."
msgstr "@1 ||..."

msgid "@1 |||.."
msgstr "@1 |||.."

msgid "@1 ||||."
msgstr "@1 ||||."

msgid "@1 |||||"
msgstr "@1 |||||"

msgid "About"
msgstr "About"

msgid "Active Lens"
msgstr "Active Lens"

msgid "Active Prism"
msgstr "Active Prism"

msgid "Additional Mods Loaded: @1"
msgstr "Additional Mods Loaded: @1"

msgid "Adobe"
msgstr "Adobe"

msgid "Adobe Bricks"
msgstr "Adobe Bricks"

msgid "Adobe Mix"
msgstr "Adobe Mix"

msgid "Aggregate"
msgstr "Aggregate"

msgid "Amalgamation"
msgstr "Amalgamation"

msgid "Annealed Lode"
msgstr "Annealed Lode"

msgid "Annealed Lode Adze"
msgstr "Annealed Lode Adze"

msgid "Annealed Lode Bar"
msgstr "Annealed Lode Bar"

msgid "Annealed Lode Frame"
msgstr "Annealed Lode Frame"

msgid "Annealed Lode Hatchet"
msgstr "Annealed Lode Hatchet"

msgid "Annealed Lode Hatchet Head"
msgstr "Annealed Lode Hatchet Head"

msgid "Annealed Lode Ladder"
msgstr "Annealed Lode Ladder"

msgid "Annealed Lode Mallet"
msgstr "Annealed Lode Mallet"

msgid "Annealed Lode Mallet Head"
msgstr "Annealed Lode Mallet Head"

msgid "Annealed Lode Mattock"
msgstr "Annealed Lode Mattock"

msgid "Annealed Lode Mattock Head"
msgstr "Annealed Lode Mattock Head"

msgid "Annealed Lode Pick"
msgstr "Annealed Lode Pick"

msgid "Annealed Lode Pick Head"
msgstr "Annealed Lode Pick Head"

msgid "Annealed Lode Prill"
msgstr "Annealed Lode Prill"

msgid "Annealed Lode Rake"
msgstr "Annealed Lode Rake"

msgid "Annealed Lode Rod"
msgstr "Annealed Lode Rod"

msgid "Annealed Lode Spade"
msgstr "Annealed Lode Spade"

msgid "Annealed Lode Spade Head"
msgstr "Annealed Lode Spade Head"

msgid "Ash"
msgstr "Ash"

msgid "Ash Lump"
msgstr "Ash Lump"

msgid "Azure Bell Flower"
msgstr "Azure Bell Flower"

msgid "Azure Cluster Flower"
msgstr "Azure Cluster Flower"

msgid "Azure Cup Flower"
msgstr "Azure Cup Flower"

msgid "Azure Rosette Flower"
msgstr "Azure Rosette Flower"

msgid "Azure Star Flower"
msgstr "Azure Star Flower"

msgid "Bindy"
msgstr "Bindy"

msgid "Bindy Adobe"
msgstr "Bindy Adobe"

msgid "Bindy Pliant Adobe"
msgstr "Bindy Pliant Adobe"

msgid "Bindy Pliant Sandstone"
msgstr "Bindy Pliant Sandstone"

msgid "Bindy Pliant Stone"
msgstr "Bindy Pliant Stone"

msgid "Bindy Pliant Tarstone"
msgstr "Bindy Pliant Tarstone"

msgid "Bindy Sandstone"
msgstr "Bindy Sandstone"

msgid "Bindy Stone"
msgstr "Bindy Stone"

msgid "Bindy Tarstone"
msgstr "Bindy Tarstone"

msgid "Black Bell Flower"
msgstr "Black Bell Flower"

msgid "Black Cluster Flower"
msgstr "Black Cluster Flower"

msgid "Black Cup Flower"
msgstr "Black Cup Flower"

msgid "Black Rosette Flower"
msgstr "Black Rosette Flower"

msgid "Black Star Flower"
msgstr "Black Star Flower"

msgid "Blank"
msgstr "Blank"

msgid "Blue Bell Flower"
msgstr "Blue Bell Flower"

msgid "Blue Cluster Flower"
msgstr "Blue Cluster Flower"

msgid "Blue Cup Flower"
msgstr "Blue Cup Flower"

msgid "Blue Rosette Flower"
msgstr "Blue Rosette Flower"

msgid "Blue Star Flower"
msgstr "Blue Star Flower"

msgid "Bonded Adobe Brick Hinged Panel"
msgstr "Bonded Adobe Brick Hinged Panel"

msgid "Bonded Adobe Brick Panel"
msgstr "Bonded Adobe Brick Panel"

msgid "Bonded Adobe Bricks"
msgstr "Bonded Adobe Bricks"

msgid "Bonded Sandstone Brick Hinged Panel"
msgstr "Bonded Sandstone Brick Hinged Panel"

msgid "Bonded Sandstone Brick Panel"
msgstr "Bonded Sandstone Brick Panel"

msgid "Bonded Sandstone Bricks"
msgstr "Bonded Sandstone Bricks"

msgid "Bonded Stone Brick Hinged Panel"
msgstr "Bonded Stone Brick Hinged Panel"

msgid "Bonded Stone Brick Panel"
msgstr "Bonded Stone Brick Panel"

msgid "Bonded Stone Bricks"
msgstr "Bonded Stone Bricks"

msgid "Bonded Tarstone Brick Hinged Panel"
msgstr "Bonded Tarstone Brick Hinged Panel"

msgid "Bonded Tarstone Brick Panel"
msgstr "Bonded Tarstone Brick Panel"

msgid "Bonded Tarstone Bricks"
msgstr "Bonded Tarstone Bricks"

msgid "Boxy"
msgstr "Boxy"

msgid "Boxy Adobe"
msgstr "Boxy Adobe"

msgid "Boxy Pliant Adobe"
msgstr "Boxy Pliant Adobe"

msgid "Boxy Pliant Sandstone"
msgstr "Boxy Pliant Sandstone"

msgid "Boxy Pliant Stone"
msgstr "Boxy Pliant Stone"

msgid "Boxy Pliant Tarstone"
msgstr "Boxy Pliant Tarstone"

msgid "Boxy Sandstone"
msgstr "Boxy Sandstone"

msgid "Boxy Stone"
msgstr "Boxy Stone"

msgid "Boxy Tarstone"
msgstr "Boxy Tarstone"

msgid "Bricky"
msgstr "Bricky"

msgid "Bricky Adobe"
msgstr "Bricky Adobe"

msgid "Bricky Pliant Adobe"
msgstr "Bricky Pliant Adobe"

msgid "Bricky Pliant Sandstone"
msgstr "Bricky Pliant Sandstone"

msgid "Bricky Pliant Stone"
msgstr "Bricky Pliant Stone"

msgid "Bricky Pliant Tarstone"
msgstr "Bricky Pliant Tarstone"

msgid "Bricky Sandstone"
msgstr "Bricky Sandstone"

msgid "Bricky Stone"
msgstr "Bricky Stone"

msgid "Bricky Tarstone"
msgstr "Bricky Tarstone"

msgid "Burning Embers"
msgstr "Burning Embers"

msgid "CHEATS ENABLED"
msgstr "CHEATS ENABLED"

msgid "Cav Charcoal Glyph"
msgstr "Cav Charcoal Glyph"

msgid "Charcoal"
msgstr "Charcoal"

msgid "Charcoal Lump"
msgstr "Charcoal Lump"

msgid "Chromatic Glass"
msgstr "Chromatic Glass"

msgid "Clear Glass"
msgstr "Clear Glass"

msgid "Clear Glass Case"
msgstr "Clear Glass Case"

msgid "Cobble"
msgstr "Cobble"

msgid "Cobble Hinged Panel"
msgstr "Cobble Hinged Panel"

msgid "Cobble Panel"
msgstr "Cobble Panel"

msgid "Cracked Stone"
msgstr "Cracked Stone"

msgid "Crafting"
msgstr "Crafting"

msgid "Crude Glass"
msgstr "Crude Glass"

msgid "DEVELOPMENT VERSION"
msgstr "DEVELOPMENT VERSION"

msgid "Dirt"
msgstr "Dirt"

msgid "Discord: https://discord.gg/NNYeF6f"
msgstr "Discord: https://discord.gg/NNYeF6f"

msgid "Discovery"
msgstr "Discovery"

msgid "Displaced Node"
msgstr "Displaced Node"

msgid "Dry Rush"
msgstr "Dry Rush"

msgid "Early-access edition of NodeCore with latest features (and maybe bugs)"
msgstr "Early-access edition of NodeCore with latest features (and maybe bugs)"

msgid "Eggcorn"
msgstr "Eggcorn"

msgid "Fire"
msgstr "Fire"

msgid "Float Glass"
msgstr "Float Glass"

msgid "Float Glass Case"
msgstr "Float Glass Case"

msgid "Flux"
msgstr "Flux"

msgid "Fot Charcoal Glyph"
msgstr "Fot Charcoal Glyph"

msgid "Gated Prism"
msgstr "Gated Prism"

msgid "Geq Charcoal Glyph"
msgstr "Geq Charcoal Glyph"

msgid "GitLab: https://gitlab.com/sztest/nodecore"
msgstr "GitLab: https://gitlab.com/sztest/nodecore"

msgid "Glowing Lode"
msgstr "Glowing Lode"

msgid "Glowing Lode Adze"
msgstr "Glowing Lode Adze"

msgid "Glowing Lode Bar"
msgstr "Glowing Lode Bar"

msgid "Glowing Lode Cobble"
msgstr "Glowing Lode Cobble"

msgid "Glowing Lode Frame"
msgstr "Glowing Lode Frame"

msgid "Glowing Lode Hatchet Head"
msgstr "Glowing Lode Hatchet Head"

msgid "Glowing Lode Ladder"
msgstr "Glowing Lode Ladder"

msgid "Glowing Lode Mallet Head"
msgstr "Glowing Lode Mallet Head"

msgid "Glowing Lode Mattock Head"
msgstr "Glowing Lode Mattock Head"

msgid "Glowing Lode Pick Head"
msgstr "Glowing Lode Pick Head"

msgid "Glowing Lode Prill"
msgstr "Glowing Lode Prill"

msgid "Glowing Lode Rake"
msgstr "Glowing Lode Rake"

msgid "Glowing Lode Rod"
msgstr "Glowing Lode Rod"

msgid "Glowing Lode Spade Head"
msgstr "Glowing Lode Spade Head"

msgid "Glued Active Lens"
msgstr "Glued Active Lens"

msgid "Glued Active Prism"
msgstr "Glued Active Prism"

msgid "Glued Gated Prism"
msgstr "Glued Gated Prism"

msgid "Glued Lens"
msgstr "Glued Lens"

msgid "Glued Prism"
msgstr "Glued Prism"

msgid "Glued Shining Lens"
msgstr "Glued Shining Lens"

msgid "Grass"
msgstr "Grass"

msgid "Gravel"
msgstr "Gravel"

msgid "Graveled Adze"
msgstr "Graveled Adze"

msgid "Growing Leaves"
msgstr "Growing Leaves"

msgid "Growing Tree Trunk"
msgstr "Growing Tree Trunk"

msgid "HAND OF POWER"
msgstr "HAND OF POWER"

msgid "Hashy"
msgstr "Hashy"

msgid "Hashy Adobe"
msgstr "Hashy Adobe"

msgid "Hashy Pliant Adobe"
msgstr "Hashy Pliant Adobe"

msgid "Hashy Pliant Sandstone"
msgstr "Hashy Pliant Sandstone"

msgid "Hashy Pliant Stone"
msgstr "Hashy Pliant Stone"

msgid "Hashy Pliant Tarstone"
msgstr "Hashy Pliant Tarstone"

msgid "Hashy Sandstone"
msgstr "Hashy Sandstone"

msgid "Hashy Stone"
msgstr "Hashy Stone"

msgid "Hashy Tarstone"
msgstr "Hashy Tarstone"

msgid "Horzy"
msgstr "Horzy"

msgid "Horzy Adobe"
msgstr "Horzy Adobe"

msgid "Horzy Pliant Adobe"
msgstr "Horzy Pliant Adobe"

msgid "Horzy Pliant Sandstone"
msgstr "Horzy Pliant Sandstone"

msgid "Horzy Pliant Stone"
msgstr "Horzy Pliant Stone"

msgid "Horzy Pliant Tarstone"
msgstr "Horzy Pliant Tarstone"

msgid "Horzy Sandstone"
msgstr "Horzy Sandstone"

msgid "Horzy Stone"
msgstr "Horzy Stone"

msgid "Horzy Tarstone"
msgstr "Horzy Tarstone"

msgid "Humus"
msgstr "Humus"

msgid "IRC: #nodecore @@ irc.libera.chat"
msgstr "IRC: #nodecore @@ irc.libera.chat"

msgid "Iceboxy"
msgstr "Iceboxy"

msgid "Iceboxy Adobe"
msgstr "Iceboxy Adobe"

msgid "Iceboxy Pliant Adobe"
msgstr "Iceboxy Pliant Adobe"

msgid "Iceboxy Pliant Sandstone"
msgstr "Iceboxy Pliant Sandstone"

msgid "Iceboxy Pliant Stone"
msgstr "Iceboxy Pliant Stone"

msgid "Iceboxy Pliant Tarstone"
msgstr "Iceboxy Pliant Tarstone"

msgid "Iceboxy Sandstone"
msgstr "Iceboxy Sandstone"

msgid "Iceboxy Stone"
msgstr "Iceboxy Stone"

msgid "Iceboxy Tarstone"
msgstr "Iceboxy Tarstone"

msgid "Infused Annealed Lode Adze"
msgstr "Infused Annealed Lode Adze"

msgid "Infused Annealed Lode Hatchet"
msgstr "Infused Annealed Lode Hatchet"

msgid "Infused Annealed Lode Mallet"
msgstr "Infused Annealed Lode Mallet"

msgid "Infused Annealed Lode Mattock"
msgstr "Infused Annealed Lode Mattock"

msgid "Infused Annealed Lode Pick"
msgstr "Infused Annealed Lode Pick"

msgid "Infused Annealed Lode Rake"
msgstr "Infused Annealed Lode Rake"

msgid "Infused Annealed Lode Spade"
msgstr "Infused Annealed Lode Spade"

msgid "Infused Tempered Lode Adze"
msgstr "Infused Tempered Lode Adze"

msgid "Infused Tempered Lode Hatchet"
msgstr "Infused Tempered Lode Hatchet"

msgid "Infused Tempered Lode Mallet"
msgstr "Infused Tempered Lode Mallet"

msgid "Infused Tempered Lode Mattock"
msgstr "Infused Tempered Lode Mattock"

msgid "Infused Tempered Lode Pick"
msgstr "Infused Tempered Lode Pick"

msgid "Infused Tempered Lode Rake"
msgstr "Infused Tempered Lode Rake"

msgid "Infused Tempered Lode Spade"
msgstr "Infused Tempered Lode Spade"

msgid "Inventory"
msgstr "Inventory"

msgid "Leaves"
msgstr "Leaves"

msgid "Lens"
msgstr "Lens"

msgid "Lit Torch"
msgstr "Lit Torch"

msgid "Living Sponge"
msgstr "Living Sponge"

msgid "Lode Cobble"
msgstr "Lode Cobble"

msgid "Lode Crate"
msgstr "Lode Crate"

msgid "Lode Form"
msgstr "Lode Form"

msgid "Lode Ore"
msgstr "Lode Ore"

msgid "Log"
msgstr "Log"

msgid "Loose Amalgamation"
msgstr "Loose Amalgamation"

msgid "Loose Cobble"
msgstr "Loose Cobble"

msgid "Loose Dirt"
msgstr "Loose Dirt"

msgid "Loose Gravel"
msgstr "Loose Gravel"

msgid "Loose Humus"
msgstr "Loose Humus"

msgid "Loose Leaves"
msgstr "Loose Leaves"

msgid "Loose Lode Cobble"
msgstr "Loose Lode Cobble"

msgid "Loose Lux Cobble"
msgstr "Loose Lux Cobble"

msgid "Loose Sand"
msgstr "Loose Sand"

msgid "Lux Cobble"
msgstr "Lux Cobble"

msgid "MIT License (http://www.opensource.org/licenses/MIT)"
msgstr "MIT License (http://www.opensource.org/licenses/MIT)"

msgid "Matrix: #nodecore:matrix.org"
msgstr "Matrix: #nodecore:matrix.org"

msgid "Mew Charcoal Glyph"
msgstr "Mew Charcoal Glyph"

msgid "Minetest's top original voxel game about emergent mechanics and exploration"
msgstr "Minetest's top original voxel game about emergent mechanics and exploration"

msgid "Molten Glass"
msgstr "Molten Glass"

msgid "Movement"
msgstr "Movement"

msgid "Niz Charcoal Glyph"
msgstr "Niz Charcoal Glyph"

msgid "NodeCore"
msgstr "NodeCore"

msgid "NodeCore ALPHA"
msgstr "NodeCore ALPHA"

msgid "Odo Charcoal Glyph"
msgstr "Odo Charcoal Glyph"

msgid "Orange Bell Flower"
msgstr "Orange Bell Flower"

msgid "Orange Cluster Flower"
msgstr "Orange Cluster Flower"

msgid "Orange Cup Flower"
msgstr "Orange Cup Flower"

msgid "Orange Rosette Flower"
msgstr "Orange Rosette Flower"

msgid "Orange Star Flower"
msgstr "Orange Star Flower"

msgid "Peat"
msgstr "Peat"

msgid "Pink Bell Flower"
msgstr "Pink Bell Flower"

msgid "Pink Cluster Flower"
msgstr "Pink Cluster Flower"

msgid "Pink Cup Flower"
msgstr "Pink Cup Flower"

msgid "Pink Rosette Flower"
msgstr "Pink Rosette Flower"

msgid "Pink Star Flower"
msgstr "Pink Star Flower"

msgid "Player's Guide: Crafting"
msgstr "Player's Guide: Crafting"

msgid "Player's Guide: Inventory Management"
msgstr "Player's Guide: Inventory Management"

msgid "Player's Guide: Movement and Navigation"
msgstr "Player's Guide: Movement and Navigation"

msgid "Player's Guide: Pummeling Recipes"
msgstr "Player's Guide: Pummeling Recipes"

msgid "Player's Guide: Tips and Guidance"
msgstr "Player's Guide: Tips and Guidance"

msgid "Pliant Adobe"
msgstr "Pliant Adobe"

msgid "Pliant Sandstone"
msgstr "Pliant Sandstone"

msgid "Pliant Stone"
msgstr "Pliant Stone"

msgid "Pliant Tarstone"
msgstr "Pliant Tarstone"

msgid "Prism"
msgstr "Prism"

msgid "Prx Charcoal Glyph"
msgstr "Prx Charcoal Glyph"

msgid "Pumice"
msgstr "Pumice"

msgid "Pummel"
msgstr "Pummel"

msgid "Pumwater"
msgstr "Pumwater"

msgid "Qeg Charcoal Glyph"
msgstr "Qeg Charcoal Glyph"

msgid "Raked Dirt"
msgstr "Raked Dirt"

msgid "Raked Gravel"
msgstr "Raked Gravel"

msgid "Raked Humus"
msgstr "Raked Humus"

msgid "Raked Sand"
msgstr "Raked Sand"

msgid "Red Bell Flower"
msgstr "Red Bell Flower"

msgid "Red Cluster Flower"
msgstr "Red Cluster Flower"

msgid "Red Cup Flower"
msgstr "Red Cup Flower"

msgid "Red Rosette Flower"
msgstr "Red Rosette Flower"

msgid "Red Star Flower"
msgstr "Red Star Flower"

msgid "Render"
msgstr "Render"

msgid "Rush"
msgstr "Rush"

msgid "Sand"
msgstr "Sand"

msgid "Sandstone"
msgstr "Sandstone"

msgid "Sandstone Bricks"
msgstr "Sandstone Bricks"

msgid "Sedge"
msgstr "Sedge"

msgid "See included LICENSE file for full details and credits"
msgstr "See included LICENSE file for full details and credits"

msgid "Shining Lens"
msgstr "Shining Lens"

msgid "Sponge"
msgstr "Sponge"

msgid "Sprout"
msgstr "Sprout"

msgid "Staff"
msgstr "Staff"

msgid "Stick"
msgstr "Stick"

msgid "Stone"
msgstr "Stone"

msgid "Stone Bricks"
msgstr "Stone Bricks"

msgid "Stone Chip"
msgstr "Stone Chip"

msgid "Stone-Tipped Hatchet"
msgstr "Stone-Tipped Hatchet"

msgid "Stone-Tipped Mallet"
msgstr "Stone-Tipped Mallet"

msgid "Stone-Tipped Pick"
msgstr "Stone-Tipped Pick"

msgid "Stone-Tipped Spade"
msgstr "Stone-Tipped Spade"

msgid "Stone-Tipped Stylus"
msgstr "Stone-Tipped Stylus"

msgid "Stump"
msgstr "Stump"

msgid "Tarstone"
msgstr "Tarstone"

msgid "Tarstone Bricks"
msgstr "Tarstone Bricks"

msgid "Tempered Lode"
msgstr "Tempered Lode"

msgid "Tempered Lode Adze"
msgstr "Tempered Lode Adze"

msgid "Tempered Lode Bar"
msgstr "Tempered Lode Bar"

msgid "Tempered Lode Frame"
msgstr "Tempered Lode Frame"

msgid "Tempered Lode Hatchet"
msgstr "Tempered Lode Hatchet"

msgid "Tempered Lode Hatchet Head"
msgstr "Tempered Lode Hatchet Head"

msgid "Tempered Lode Ladder"
msgstr "Tempered Lode Ladder"

msgid "Tempered Lode Mallet"
msgstr "Tempered Lode Mallet"

msgid "Tempered Lode Mallet Head"
msgstr "Tempered Lode Mallet Head"

msgid "Tempered Lode Mattock"
msgstr "Tempered Lode Mattock"

msgid "Tempered Lode Mattock Head"
msgstr "Tempered Lode Mattock Head"

msgid "Tempered Lode Pick"
msgstr "Tempered Lode Pick"

msgid "Tempered Lode Pick Head"
msgstr "Tempered Lode Pick Head"

msgid "Tempered Lode Prill"
msgstr "Tempered Lode Prill"

msgid "Tempered Lode Rake"
msgstr "Tempered Lode Rake"

msgid "Tempered Lode Rod"
msgstr "Tempered Lode Rod"

msgid "Tempered Lode Spade"
msgstr "Tempered Lode Spade"

msgid "Tempered Lode Spade Head"
msgstr "Tempered Lode Spade Head"

msgid "Thatch"
msgstr "Thatch"

msgid "The discovery system only alerts you to the existence of some basic game mechanics. More advanced content, such as emergent systems and automation, you will have to invent yourself!"
msgstr "The discovery system only alerts you to the existence of some basic game mechanics. More advanced content, such as emergent systems and automation, you will have to invent yourself!"

msgid "Tips"
msgstr "Tips"

msgid "Tof Charcoal Glyph"
msgstr "Tof Charcoal Glyph"

msgid "Torch"
msgstr "Torch"

msgid "Tote (@1 / @2)"
msgstr "Tote (@1 / @2)"

msgid "Tote Handle"
msgstr "Tote Handle"

msgid "Tree Trunk"
msgstr "Tree Trunk"

msgid "Vermy"
msgstr "Vermy"

msgid "Vermy Adobe"
msgstr "Vermy Adobe"

msgid "Vermy Pliant Adobe"
msgstr "Vermy Pliant Adobe"

msgid "Vermy Pliant Sandstone"
msgstr "Vermy Pliant Sandstone"

msgid "Vermy Pliant Stone"
msgstr "Vermy Pliant Stone"

msgid "Vermy Pliant Tarstone"
msgstr "Vermy Pliant Tarstone"

msgid "Vermy Sandstone"
msgstr "Vermy Sandstone"

msgid "Vermy Stone"
msgstr "Vermy Stone"

msgid "Vermy Tarstone"
msgstr "Vermy Tarstone"

msgid "Verty"
msgstr "Verty"

msgid "Verty Adobe"
msgstr "Verty Adobe"

msgid "Verty Pliant Adobe"
msgstr "Verty Pliant Adobe"

msgid "Verty Pliant Sandstone"
msgstr "Verty Pliant Sandstone"

msgid "Verty Pliant Stone"
msgstr "Verty Pliant Stone"

msgid "Verty Pliant Tarstone"
msgstr "Verty Pliant Tarstone"

msgid "Verty Sandstone"
msgstr "Verty Sandstone"

msgid "Verty Stone"
msgstr "Verty Stone"

msgid "Verty Tarstone"
msgstr "Verty Tarstone"

msgid "Violet Bell Flower"
msgstr "Violet Bell Flower"

msgid "Violet Cluster Flower"
msgstr "Violet Cluster Flower"

msgid "Violet Cup Flower"
msgstr "Violet Cup Flower"

msgid "Violet Rosette Flower"
msgstr "Violet Rosette Flower"

msgid "Violet Star Flower"
msgstr "Violet Star Flower"

msgid "Water"
msgstr "Water"

msgid "Wet Adobe Mix"
msgstr "Wet Adobe Mix"

msgid "Wet Aggregate"
msgstr "Wet Aggregate"

msgid "Wet Render"
msgstr "Wet Render"

msgid "Wet Sponge"
msgstr "Wet Sponge"

msgid "Wet Tarry Aggregate"
msgstr "Wet Tarry Aggregate"

msgid "White Bell Flower"
msgstr "White Bell Flower"

msgid "White Cluster Flower"
msgstr "White Cluster Flower"

msgid "White Cup Flower"
msgstr "White Cup Flower"

msgid "White Rosette Flower"
msgstr "White Rosette Flower"

msgid "White Star Flower"
msgstr "White Star Flower"

msgid "Wicker"
msgstr "Wicker"

msgid "Wilted Bell Flower"
msgstr "Wilted Bell Flower"

msgid "Wilted Cluster Flower"
msgstr "Wilted Cluster Flower"

msgid "Wilted Cup Flower"
msgstr "Wilted Cup Flower"

msgid "Wilted Rosette Flower"
msgstr "Wilted Rosette Flower"

msgid "Wilted Star Flower"
msgstr "Wilted Star Flower"

msgid "Wooden Adze"
msgstr "Wooden Adze"

msgid "Wooden Form"
msgstr "Wooden Form"

msgid "Wooden Frame"
msgstr "Wooden Frame"

msgid "Wooden Hatchet"
msgstr "Wooden Hatchet"

msgid "Wooden Hatchet Head"
msgstr "Wooden Hatchet Head"

msgid "Wooden Hinged Panel"
msgstr "Wooden Hinged Panel"

msgid "Wooden Ladder"
msgstr "Wooden Ladder"

msgid "Wooden Mallet"
msgstr "Wooden Mallet"

msgid "Wooden Mallet Head"
msgstr "Wooden Mallet Head"

msgid "Wooden Panel"
msgstr "Wooden Panel"

msgid "Wooden Pick"
msgstr "Wooden Pick"

msgid "Wooden Pick Head"
msgstr "Wooden Pick Head"

msgid "Wooden Plank"
msgstr "Wooden Plank"

msgid "Wooden Rake"
msgstr "Wooden Rake"

msgid "Wooden Shelf"
msgstr "Wooden Shelf"

msgid "Wooden Spade"
msgstr "Wooden Spade"

msgid "Wooden Spade Head"
msgstr "Wooden Spade Head"

msgid "Xrp Charcoal Glyph"
msgstr "Xrp Charcoal Glyph"

msgid "Yellow Bell Flower"
msgstr "Yellow Bell Flower"

msgid "Yellow Cluster Flower"
msgstr "Yellow Cluster Flower"

msgid "Yellow Cup Flower"
msgstr "Yellow Cup Flower"

msgid "Yellow Rosette Flower"
msgstr "Yellow Rosette Flower"

msgid "Yellow Star Flower"
msgstr "Yellow Star Flower"

msgid "Yit Charcoal Glyph"
msgstr "Yit Charcoal Glyph"

msgid "Zin Charcoal Glyph"
msgstr "Zin Charcoal Glyph"

msgid "activate a lens"
msgstr "activate a lens"

msgid "activate a prism"
msgstr "activate a prism"

msgid "add coal to aggregate to make tarstone"
msgstr "add coal to aggregate to make tarstone"

msgid "anneal a lode cube"
msgstr "anneal a lode cube"

msgid "assemble a clear glass case"
msgstr "assemble a clear glass case"

msgid "assemble a float glass case"
msgstr "assemble a float glass case"

msgid "assemble a lode adze"
msgstr "assemble a lode adze"

msgid "assemble a lode crate from form and bar"
msgstr "assemble a lode crate from form and bar"

msgid "assemble a lode rake"
msgstr "assemble a lode rake"

msgid "assemble a lode tote handle"
msgstr "assemble a lode tote handle"

msgid "assemble a rake from adzes and a staff"
msgstr "assemble a rake from adzes and a staff"

msgid "assemble a staff from sticks"
msgstr "assemble a staff from sticks"

msgid "assemble a stone-tipped stylus"
msgstr "assemble a stone-tipped stylus"

msgid "assemble a wooden frame from staves"
msgstr "assemble a wooden frame from staves"

msgid "assemble a wooden ladder from sticks"
msgstr "assemble a wooden ladder from sticks"

msgid "assemble a wooden shelf from a form and plank"
msgstr "assemble a wooden shelf from a form and plank"

msgid "assemble a wooden tool"
msgstr "assemble a wooden tool"

msgid "assemble an adze out of sticks"
msgstr "assemble an adze out of sticks"

msgid "bash a plank into sticks"
msgstr "bash a plank into sticks"

msgid "bond stone bricks"
msgstr "bond stone bricks"

msgid "break cobble into chips"
msgstr "break cobble into chips"

msgid "breed a new flower variety"
msgstr "breed a new flower variety"

msgid "carve a wooden plank completely"
msgstr "carve a wooden plank completely"

msgid "carve wooden tool heads from planks"
msgstr "carve wooden tool heads from planks"

msgid "catapult an item with a hinged panel"
msgstr "catapult an item with a hinged panel"

msgid "change a stylus pattern"
msgstr "change a stylus pattern"

msgid "chip chromatic glass into prisms"
msgstr "chip chromatic glass into prisms"

msgid "chisel a hinge groove into a wooden plank"
msgstr "chisel a hinge groove into a wooden plank"

msgid "chisel a hinge groove into cobble"
msgstr "chisel a hinge groove into cobble"

msgid "chisel stone bricks"
msgstr "chisel stone bricks"

msgid "chop chromatic glass into lenses"
msgstr "chop chromatic glass into lenses"

msgid "chop up charcoal"
msgstr "chop up charcoal"

msgid "complete a pummel with a hinged panel and tool head"
msgstr "complete a pummel with a hinged panel and tool head"

msgid "complete an assembly recipe with a hinged panel"
msgstr "complete an assembly recipe with a hinged panel"

msgid "compress something with a hinged panel"
msgstr "compress something with a hinged panel"

msgid "convert a wooden form to a frame"
msgstr "convert a wooden form to a frame"

msgid "convert a wooden frame to a form"
msgstr "convert a wooden frame to a form"

msgid "cool molten glass into crude glass"
msgstr "cool molten glass into crude glass"

msgid "craft a torch from staff and coal lump"
msgstr "craft a torch from staff and coal lump"

msgid "cure pliant concrete fully"
msgstr "cure pliant concrete fully"

msgid "cut down a tree"
msgstr "cut down a tree"

msgid "cycle through all charcoal glyphs"
msgstr "cycle through all charcoal glyphs"

msgid "dig a node with a hinged panel and tool"
msgstr "dig a node with a hinged panel and tool"

msgid "dig leaves"
msgstr "dig leaves"

msgid "dig up a tree stump"
msgstr "dig up a tree stump"

msgid "dig up cobble"
msgstr "dig up cobble"

msgid "dig up dirt"
msgstr "dig up dirt"

msgid "dig up gravel"
msgstr "dig up gravel"

msgid "dig up lode ore"
msgstr "dig up lode ore"

msgid "dig up lux cobble"
msgstr "dig up lux cobble"

msgid "dig up sand"
msgstr "dig up sand"

msgid "discovered - @1"
msgstr "discovered - @1"

msgid "drop all your items at once"
msgstr "drop all your items at once"

msgid "drop an item"
msgstr "drop an item"

msgid "dry out a rush"
msgstr "dry out a rush"

msgid "dry out a sponge"
msgstr "dry out a sponge"

msgid "etch pliant concrete with a stylus"
msgstr "etch pliant concrete with a stylus"

msgid "ferment peat into humus"
msgstr "ferment peat into humus"

msgid "find a flower"
msgstr "find a flower"

msgid "find a rush"
msgstr "find a rush"

msgid "find a sedge"
msgstr "find a sedge"

msgid "find a sponge"
msgstr "find a sponge"

msgid "find a stick"
msgstr "find a stick"

msgid "find an eggcorn"
msgstr "find an eggcorn"

msgid "find ash"
msgstr "find ash"

msgid "find charcoal"
msgstr "find charcoal"

msgid "find deep stone strata"
msgstr "find deep stone strata"

msgid "find dry (loose) leaves"
msgstr "find dry (loose) leaves"

msgid "find lode ore"
msgstr "find lode ore"

msgid "find lux"
msgstr "find lux"

msgid "find pumice"
msgstr "find pumice"

msgid "find pumwater"
msgstr "find pumwater"

msgid "forge a lode prill into a bar"
msgstr "forge a lode prill into a bar"

msgid "forge a lode rod and bar into a ladder"
msgstr "forge a lode rod and bar into a ladder"

msgid "forge an annealed lode frame into a form"
msgstr "forge an annealed lode frame into a form"

msgid "forge lode bars into a rod"
msgstr "forge lode bars into a rod"

msgid "forge lode prills into a tool head"
msgstr "forge lode prills into a tool head"

msgid "forge lode rods into a frame"
msgstr "forge lode rods into a frame"

msgid "gate a prism"
msgstr "gate a prism"

msgid "go for a swim"
msgstr "go for a swim"

msgid "grind dead plants into peat"
msgstr "grind dead plants into peat"

msgid "grow a flower on moist soil"
msgstr "grow a flower on moist soil"

msgid "grow a rush on moist soil"
msgstr "grow a rush on moist soil"

msgid "grow a sedge on moist grass"
msgstr "grow a sedge on moist grass"

msgid "harden stone"
msgstr "harden stone"

msgid "harvest a sponge"
msgstr "harvest a sponge"

msgid "hold your breath"
msgstr "hold your breath"

msgid "https://content.minetest.net/packages/Warr1024/nodecore/"
msgstr "https://content.minetest.net/packages/Warr1024/nodecore/"

msgid "insert metal rod into a cobble panel"
msgstr "insert metal rod into a cobble panel"

msgid "insert wooden pin into wooden panel"
msgstr "insert wooden pin into wooden panel"

msgid "leach raked dirt to sand"
msgstr "leach raked dirt to sand"

msgid "leach raked humus to dirt"
msgstr "leach raked humus to dirt"

msgid "light a torch"
msgstr "light a torch"

msgid "lux-infuse a lode tool"
msgstr "lux-infuse a lode tool"

msgid "make fire by rubbing sticks together"
msgstr "make fire by rubbing sticks together"

msgid "melt down lode metal from lode cobble"
msgstr "melt down lode metal from lode cobble"

msgid "melt sand into molten glass"
msgstr "melt sand into molten glass"

msgid "melt stone into pumwater"
msgstr "melt stone into pumwater"

msgid "mix dirt into ash to make adobe mix"
msgstr "mix dirt into ash to make adobe mix"

msgid "mix gravel into ash to make aggregate"
msgstr "mix gravel into ash to make aggregate"

msgid "mix sand into ash to make render"
msgstr "mix sand into ash to make render"

msgid "mold molten glass into clear glass"
msgstr "mold molten glass into clear glass"

msgid "mold molten glass into float glass"
msgstr "mold molten glass into float glass"

msgid "navigate by touch in darkness"
msgstr "navigate by touch in darkness"

msgid "observe a lux reaction"
msgstr "observe a lux reaction"

msgid "observe lux criticality"
msgstr "observe lux criticality"

msgid "pack dry rushes into wicker"
msgstr "pack dry rushes into wicker"

msgid "pack high-quality charcoal"
msgstr "pack high-quality charcoal"

msgid "pack sedges into thatch"
msgstr "pack sedges into thatch"

msgid "pack stone chips back into cobble"
msgstr "pack stone chips back into cobble"

msgid "pack up a complete tote"
msgstr "pack up a complete tote"

msgid "pick a sedge"
msgstr "pick a sedge"

msgid "place a node with a hinged panel"
msgstr "place a node with a hinged panel"

msgid "plant an eggcorn"
msgstr "plant an eggcorn"

msgid "produce light from a lens"
msgstr "produce light from a lens"

msgid "propel hinged panel with focused light"
msgstr "propel hinged panel with focused light"

msgid "push an item into a storage box with a hinged panel"
msgstr "push an item into a storage box with a hinged panel"

msgid "put a gravel tip on a wooden adze"
msgstr "put a gravel tip on a wooden adze"

msgid "put a stone tip onto a wooden tool"
msgstr "put a stone tip onto a wooden tool"

msgid "quench molten glass into chromatic glass"
msgstr "quench molten glass into chromatic glass"

msgid "quench pumwater to amalgamation"
msgstr "quench pumwater to amalgamation"

msgid "rake dirt"
msgstr "rake dirt"

msgid "rake gravel"
msgstr "rake gravel"

msgid "rake humus"
msgstr "rake humus"

msgid "rake sand"
msgstr "rake sand"

msgid "rotate a charcoal glyph"
msgstr "rotate a charcoal glyph"

msgid "run at full speed"
msgstr "run at full speed"

msgid "scale a wall"
msgstr "scale a wall"

msgid "scale an overhang"
msgstr "scale an overhang"

msgid "see a tree grow"
msgstr "see a tree grow"

msgid "set concrete to pliant"
msgstr "set concrete to pliant"

msgid "sinter glowing lode prills into a cube"
msgstr "sinter glowing lode prills into a cube"

msgid "split a tree trunk into planks"
msgstr "split a tree trunk into planks"

msgid "squeeze out a sponge"
msgstr "squeeze out a sponge"

msgid "stick a lens/prism in place"
msgstr "stick a lens/prism in place"

msgid "temper a lode cube"
msgstr "temper a lode cube"

msgid "temper a lode tool head"
msgstr "temper a lode tool head"

msgid "throw an item really fast"
msgstr "throw an item really fast"

msgid "weaken stone by soaking"
msgstr "weaken stone by soaking"

msgid "weld glowing lode pick and spade heads together"
msgstr "weld glowing lode pick and spade heads together"

msgid "wet a concrete mix"
msgstr "wet a concrete mix"

msgid "wilt a flower"
msgstr "wilt a flower"

msgid "work annealed lode on a tempered lode anvil"
msgstr "work annealed lode on a tempered lode anvil"

msgid "work glowing lode on a lode anvil"
msgstr "work glowing lode on a lode anvil"

msgid "work glowing lode on a stone anvil"
msgstr "work glowing lode on a stone anvil"

msgid "write on a surface with a charcoal lump"
msgstr "write on a surface with a charcoal lump"

