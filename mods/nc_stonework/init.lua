-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("adze")
include("chip")
include("tools")
include("bricks")
include("dungeon")
include("hints")
